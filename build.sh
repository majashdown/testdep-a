# Definitions

vfile=dep/version
idir=dep/inputs
odir=dep/outputs
project=${CI_PROJECT_NAME}

# Version number

version=$(cat $vfile)

echo
echo version: $version
echo

# List inputs

ifile=$(ls -A1 $idir)

echo inputs:
echo $ifiles
echo

ifiles=$(ls -A1 $idir)
for i in $ifiles; do
    echo $i:
    cat $idir/$i
    echo
done

# Build

echo $version > $odir/$project

# List outputs

ofiles=$(ls -A1 $odir)

echo outputs:
echo $ofiles
echo

for i in $ofiles; do
    echo $i:
    cat $odir/$i
    echo
done
